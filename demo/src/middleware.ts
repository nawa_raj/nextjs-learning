import { NextRequest } from "next/server";

// EXPORT MIDDLEWARE CONFIG FILE FOR MATCHER....
export const config = {
  matcher: [
    /*
     * Match all request paths except for the ones starting with:
     * - api (API routes)
     * - _next/static (static files)
     * - favicon.ico (favicon file)
     */
    "/((?!api|_next/static|favicon.ico).*)",
  ],
};


// MIDDLEWARE TO CONTROL SOMETHINGS...
export default async function middleware(req: NextRequest) {

  // console.log("\n\n--------------------------- All Logs received from Request ----------------------")
  // console.log("req.geo:\n ", req.geo)
  // console.log("req.ip\n: ", req.ip)
  // console.log("req.credentials:\n", req.credentials)
  // console.log("req.headers.get('user-agent'):\n", req.headers.get('user-agent'))

  // console.log("\n------------------\nRequest\n---------------------\n: ", req);

}
