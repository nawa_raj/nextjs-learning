import '../styles/globals.css'
import type { AppProps } from 'next/app';
import React, { ReactElement, ReactNode } from 'react';
import { NextPage } from 'next';
import Layout from '../components/Layout';
import GetLayout from '../components/GetLayout';


// TYPE DECLEARATION...
export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
  getLayout?: (page: ReactElement) => ReactNode
}
type AppPropsWithLayout = AppProps & { Component: NextPageWithLayout };



// -------------------------------------------------------------------------
function MyApp({ Component, pageProps }: AppPropsWithLayout) {

  // Use the layout defined at the page level, if available
  if (Component.getLayout) return Component.getLayout(<GetLayout><Component {...pageProps} /></GetLayout>);

  // using default layout
  return <Layout> <Component {...pageProps} /> </Layout>
}

export default MyApp
