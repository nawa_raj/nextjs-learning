import Head from "next/head"


const About = () => {
    return (
        <div>
            <Head>
                <title>Demo | About Page </title>
                <meta name="description" content="this is about page metadata" />
            </Head>

            <p>Welcome in About Page</p>
        </div>
    )
}

export default About;
