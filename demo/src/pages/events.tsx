import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';
import React, { useState } from 'react'
import { jsonServerBaseUrl } from '../services/config';



// -----------------------------------------------------------------------------------------
//  this example demonstrate server side and client side data fetching as combined
// -----------------------------------------------------------------------------------------


export interface eventsModel {
    id: number;
    title: string;
    description: string;
    category: string;
    date: string;
}

interface Props { eventsList: Array<eventsModel> }

const displayFlex = { display: "flex", justifyContent: "space-between", alignItems: "flex-end" };


const EventsLists = ({ eventsList }: Props) => {
    const router = useRouter();
    const [events, setEvents] = useState(eventsList);

    const handleFilter = async (query: string) => {
        const response = await fetch(`${jsonServerBaseUrl}/events?category=${query}`);
        const data = await response.json();
        if (response.ok && data) {
            setEvents(data);
            router.push(`/events?category=${query}`, undefined, { shallow: true });
        }
    }


    const handleClearFilter = async () => {
        const response = await fetch(`${jsonServerBaseUrl}/events`);
        const data = await response.json();

        if (response.ok && data) {
            setEvents(data);
            router.push("/events");
        }

    }


    return (
        <div style={{ padding: "1rem" }}>
            <h2>Events lists: </h2>

            <div style={displayFlex}>
                <div style={{ ...displayFlex, flexGrow: 1, justifyContent: "flex-start" }}>
                    <p style={{ marginRight: "1rem" }}><strong>Filter Options: </strong></p>
                    <button onClick={handleClearFilter}>Clear Filter</button>
                </div>
                <div style={displayFlex}>
                    <button onClick={() => handleFilter("sports")}>Sports</button>
                    <button onClick={() => handleFilter("technology")} style={{ margin: "0 0.5rem" }}>Technology</button>
                    <button onClick={() => handleFilter("foods")}>Foods</button>
                </div>
            </div>

            {events.map((event, ind) => {
                return (
                    <div key={event.id} style={{ padding: "0 1rem" }}>
                        <h4>{event.title} {event.date} | {event.category}</h4>
                        <p>{event.description}</p>
                        <hr />
                    </div>
                )
            })}
        </div>
    )
}

export default EventsLists;



// SERVER SIDE THINGS...
export const getServerSideProps: GetServerSideProps = async ({ query }) => {
    const category = query.category ? `category=${query.category}` : "";

    const response = await fetch(`${jsonServerBaseUrl}/events?${category}`);
    const data = await response.json();

    if (response.ok && data) return { props: { eventsList: data } };
    else return { notFound: true };
}