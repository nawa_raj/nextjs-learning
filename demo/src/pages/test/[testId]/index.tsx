import { GetServerSideProps } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { ParsedUrlQuery } from 'querystring';
import React, { FC } from 'react'


const TestIdIndexPage: FC<{ params: ParsedUrlQuery, query: ParsedUrlQuery }> = ({ params, query }) => {
    const router = useRouter();

    // console.log("is ready: ", router.isReady)

    return (
        <div style={{ padding: "1rem" }}>
            <h3>This is a test list page its hold its child routes...</h3>

            <div style={{ marginTop: "1rem" }}>
                <strong>params :</strong>
                <div>{JSON.stringify(params)}</div>
            </div>

            <div style={{ marginTop: "1rem" }}>
                <strong>query :</strong>
                <div>{JSON.stringify(query)}</div>
            </div>

            <div style={{ margin: "0.5rem 0.5rem 0.5rem 0" }}><strong>Link to visit my child routes</strong></div>
            <div style={{ marginTop: "1rem" }}>
                <Link href={{ pathname: `${router.query.testId}/test-description`, query: "hello=how are you" }}>test description</Link>
            </div>
        </div>
    )
}

export default TestIdIndexPage;


// ------------------------- SERVER THINGS ----------------------------------
export const getServerSideProps: GetServerSideProps = async ({ params, query, req }) => {
    // console.log("params: ", params)
    // console.log("query: ", query)

    const forwarded = req.headers["x-forwarded-for"]
    const ip = forwarded ? String(forwarded).split(/, /)[0] : req.connection.remoteAddress

    console.log("IP: ", ip)
    console.log("req.headers['x - forwarded -for']: ", forwarded)
    console.log("req.headers.forwarded: ", req.headers.forwarded)

    return {
        props: { params, query }
    }
}