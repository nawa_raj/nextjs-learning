import { useRouter } from 'next/router';
import React from 'react'



const TestDetailPage = () => {
    const router = useRouter();
    const { query, asPath, pathname, isReady } = router;

    // console.log("isready: ", isReady)


    return (
        <div style={{ padding: "1rem" }}>
            <h3>This is a test detail page make for testing things...</h3>

            <div style={{ marginTop: "1rem" }}>
                <strong>query :</strong>
                <div>{JSON.stringify(query)}</div>
            </div>

            <div style={{ marginTop: "1rem" }}>
                <strong>as path :</strong>
                <div>{JSON.stringify(asPath)}</div>
            </div>

            <div style={{ marginTop: "1rem" }}>
                <strong>path name:</strong>
                <div>{JSON.stringify(pathname)}</div>
            </div>
        </div>
    )
}

export default TestDetailPage;
