import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';


const TestIndexPage = () => {
    const router = useRouter()

    return (
        <div>
            <h2>This is a Test Index Page</h2>
            <button onClick={() => router.push("/test/1")}>Go to next page </button>

            <div style={{ marginTop: "1rem" }}>
                <div style={{ margin: "0.5rem 0.5rem 0.5rem 0" }}><strong>create link with encodeURIComponent object </strong></div>

                {/* this is a URI encoded version */}
                <Link href={{ pathname: `/test/${encodeURIComponent("post-title?id=4")}` }}>
                    link
                </Link>

                <div style={{ margin: "0.5rem 0.5rem 0.5rem 0" }}><strong>Create link without encodeURIComponent object</strong></div>
                <Link href={{ pathname: `/test/post-title`, query: "id=4" }}>
                    link
                </Link>
            </div>
        </div>
    )
}

export default TestIndexPage