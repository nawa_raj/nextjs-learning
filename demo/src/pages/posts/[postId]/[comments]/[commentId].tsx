import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router';
import React from 'react'
import { postCommentDataType } from '.';


interface Props { comment: postCommentDataType }


const CommentDetailPage = ({ comment }: Props) => {
    const router = useRouter();


    // console.log("pathname: ", router.pathname);
    console.log("route: ", router.route);
    console.log("query: ", router.query);


    return (
        <div>
            <h4>Comment Name: {comment.name}</h4>
            <p>Comment By: {comment.email}</p>
            <p>Comment On: {comment.postId}</p>
            <p>Comment Body: {comment.body}</p>
        </div>
    )
}

export default CommentDetailPage;

type paramstype = {
    postId: number;
    commentId: number;
}
export const getServerSideProps: GetServerSideProps = async ({ params, query }) => {
    const { postId, commentId } = (params as any) as paramstype;

    const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${postId}/comments`);
    const comments = await res.json();

    // console.log("params: ", params);
    // console.log("comments: ", comments)
    // console.log("object: ", comments[commentId])
    // console.log("query: ", query)

    return {
        props: { comment: comments[commentId] }
    }
}