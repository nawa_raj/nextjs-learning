import { GetServerSideProps } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react'
import style from "../../../../styles/posts.module.css";


export interface postCommentDataType {
    postId: number;
    id: number;
    name: string;
    email: string;
    body: string;
}


interface Props {
    comments: postCommentDataType[];
}

const PostCommentsPage = ({ comments }: Props) => {
    const router = useRouter();
    const { postId } = router.query;


    return (
        <div style={{ width: "50%", background: "#e0e0e0", padding: "1rem" }}>
            {
                comments.map((comment, ind) => (
                    <div key={comment.id} style={{ marginBottom: "10px", paddingBottom: "10px", borderBottom: "1px solid purple" }}>
                        <h4> {comment.name}</h4>
                        <p>Post Id: {comment.postId}</p>
                        <p>Comment By: {comment.email}</p>

                        <div style={{ display: "flex", justifyContent: "flex-end", marginTop: "0.5rem" }}>
                            <Link href={`/posts/${postId}/comments/${ind}`} className={style.view_comment}>
                                View Comments Details
                            </Link>
                        </div>
                    </div>
                ))
            }
        </div>
    )
}

export default PostCommentsPage;


export const getServerSideProps: GetServerSideProps = async ({ params }) => {
    const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${params?.postId}/comments`);
    const comments = await res.json();

    return {
        props: { comments }
    }
}