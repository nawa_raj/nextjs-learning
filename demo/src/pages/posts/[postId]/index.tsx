import { GetStaticPaths, GetStaticProps } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react'
import { postsType } from '../';
import Loading from '../../../components/Loading';
import style from "../../../styles/posts.module.css";


// 
interface Props { post: postsType };

const POST_ID = /^[0-9]+$/;


const PostDetailPage = ({ post }: Props) => {
    const router = useRouter();
    const { postId } = router.query;

    if (router.isFallback) return <Loading />;

    return (
        <div className={style.post_detail_container}>
            <h3>{post.title}</h3>
            <p>post id:  {post.id}</p>
            <p>post Body:  {post.body}</p>
            <p>created by: {post.userId}</p>

            <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <Link href={`/posts/${postId}/comments`} className={style.view_comment}>
                    View all comments
                </Link>
            </div>

            <Link href="/" style={{ marginTop: "1rem", color: "blue" }}>Back to Home</Link>
        </div>
    )
}

export default PostDetailPage



// ------------------ NEXT SERVER THINGS ----------------------------------
export const getStaticPaths: GetStaticPaths = async ({ }) => {
    // const res = await fetch("https://jsonplaceholder.typicode.com/posts", { headers: new Headers({ authorization: "Bearer " }) });
    // const posts: postsType[] = await res.json();

    // const paths = posts.map((post) => { return { params: { postId: `${post.id}` } } };
    const paths = [{ params: { postId: '1' } }, { params: { postId: '2' } }, { params: { postId: '3' } }];

    return {
        paths: paths,

        // fallback: false
        // if fallback = false then, only generated page at build time will be render
        // if page not found then it will render 404 page 

        fallback: true,
        // when fallback = true then on new request postId is matched
        // if not found then it will call the api again to find postId
        // if found it return the post id, if not fount it return blank postId page with no dynamic data and make fallback true

        // fallback: 'blocking',
        // if fallback = 'blocking' then on new request is matched with generated pages
        // if not found then it will search in api and then return page if param found 
        // if it not found any param then it will not render 404 page and make fallback = true;
    }
}


export const getStaticProps: GetStaticProps = async ({ params }) => {
    const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${params?.postId}`);
    const data = await res.json();

    console.log(`Generating page for /posts/${params?.postId}`);


    // VALIDATING postId...
    if (res.ok && data.id) return { props: { post: data } }
    else return { notFound: true }
}
