import { GetStaticProps } from 'next';
import Link from 'next/link';
import React from 'react'
import style from "../../styles/posts.module.css";



// -----------------------------------------------------------------------------
//  this example demonstrate the static site generation
// -----------------------------------------------------------------------------

export interface postsType {
    userId: number;
    id: number;
    title: string;
    body: string;
}
interface Props { posts: Array<postsType> };



// example of dynamic routes...

const JobsIndexPage = ({ posts }: Props) => {

    return (
        <div className={style.container}>
            <ul>
                {
                    posts.map((post) => {
                        return (
                            <li key={post.id.toString()}>
                                {post.id}. <Link href={`/posts/${post.id}`} legacyBehavior>{post.title}</Link>
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    )
};

export default JobsIndexPage;




// ------------------ NEXT SERVER THINGS ----------------------------------
export const getStaticProps: GetStaticProps = async ({ params }) => {
    // console.log("params: ", params);

    const res = await fetch("https://jsonplaceholder.typicode.com/posts", { headers: new Headers({ authorization: "Bearer " }) });
    const data: postsType[] = await res.json();

    return { props: { posts: data.slice(0, 4) } }
}