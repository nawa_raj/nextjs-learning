import React, { ReactElement } from 'react'

const Login = () => {
    return (
        <div>this is Login page without default Layout</div>
    )
}


// OVERRIDE DEFAULT LAYOUT
Login.getLayout = function getLayout(page: ReactElement) {
    return <>{page}</>
};


export default Login;