import Image from 'next/image';
import React from 'react';


const openImages = [
    "https://bit.ly/3ERbWNX",
    "https://bit.ly/3iy4Fve",
    "https://bit.ly/3uhMwEq",
    "https://bit.ly/3XNgNsg",
    "https://bit.ly/3OUmq3C",
]

const PetsIndexPage = () => {
    return (
        <div className="displayflex">
            <div style={{ width: "40%" }}>
                <h2>Local Images</h2>
                <div style={{ background: "#f0f0f0", padding: "1rem", display: "flex", alignItems: "center", flexWrap: "wrap" }}>
                    {
                        ["1", "2", "3", "4", "5"].map(path => {
                            return (
                                <div key={path} style={{ margin: "0.2rem", }}>
                                    <Image
                                        src={`/images/pets/${path}.jpg`}
                                        alt="pet"
                                        width={120}
                                        height={120}
                                        style={{ objectFit: "contain", borderRadius: "4px" }}
                                        placeholder="blur"
                                        blurDataURL={`/images/pets/${path}.jpg`}
                                    />
                                </div>
                            )
                        })
                    }
                </div>
            </div>

            <div style={{ width: "40%" }}>
                <h2>images From Other Link Source</h2>
                <div style={{ background: "#f0f0f0", padding: "1rem", display: "flex", alignItems: "center", flexWrap: "wrap" }}>
                    {
                        openImages.map((url, ind) => {
                            return (
                                <div key={ind} style={{ position: "relative", margin: "0.2rem", width: "25%", height: "110px", }}>
                                    <Image
                                        src={url}
                                        alt="pet"
                                        layout='fill'
                                        style={{ objectFit: "contain", borderRadius: "4px" }}
                                        placeholder="blur"
                                        blurDataURL={url}
                                    />
                                </div>
                            )
                        })
                    }

                    <img src={"https://photos.app.goo.gl/hVbviXm1kAfi3P2w6"} alt="pet" height={120} style={{ objectFit: "contain", borderRadius: "4px" }} />
                </div>
            </div>
        </div>
    )
}

export default PetsIndexPage