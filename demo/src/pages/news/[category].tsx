import { GetServerSideProps } from 'next';
import React from 'react'
import { newsModel } from '.';
import { jsonServerBaseUrl } from '../../services/config';


// TYPE DECLERATION...
interface Props {
    category: string,
    articles: newsModel[];
}

const NewsByCategory = ({ category, articles }: Props) => {
    return (
        <div style={{ padding: "1rem" }}>
            <p>Showing result for <strong>{category}{" "}</strong>Category</p>
            <hr />

            <div style={{ padding: "1rem" }}>
                {articles.map((article, ind) => {
                    return (
                        <div key={article.id}>
                            <span>{ind + 1}.</span>
                            <h4 style={{ display: "inline-flex", margin: "2px 0px 2px 5px" }}>{article.title}</h4>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default NewsByCategory;


// SERVER SIDE THINGS...
export const getServerSideProps: GetServerSideProps = async ({ params }) => {
    console.log("params: ", params);

    const searchquery = params?.category ? `category=${params.category}` : "";
    const response = await fetch(`${jsonServerBaseUrl}/news?${searchquery}`);
    const articles = await response.json();


    if (response.ok && (Array.isArray(articles) && articles.length)) {
        return {
            props: { category: params?.category, articles }
        }
    } else {
        return { notFound: true }
    }

}