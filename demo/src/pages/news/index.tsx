import { GetServerSideProps } from 'next';
import Link from 'next/link';
import React from 'react';
import { jsonServerBaseUrl } from '../../services/config';


// -----------------------------------------------------------------------------
//  this example demonstrate the server side rendering at request time
// -----------------------------------------------------------------------------


// TYPE DECLERATION...
export interface newsModel {
    id: 1;
    title: string;
    description: string;
    category: string
}

interface Props {
    articles: Array<newsModel>
}


function NewsArticalLists({ articles }: Props) {
    return (
        <div style={{ marginLeft: "1rem", padding: "1rem" }}>
            <h1 style={{ marginBottom: ".5rem" }}>Lists of Articles: </h1>
            {
                articles.map((article) => {
                    return (
                        <div key={article.id}>
                            <Link href={`/news/${article.category}`} >
                                <div>
                                    <span>{article.id}.</span>
                                    <h4 style={{ display: "inline-flex", margin: "2px 0px 2px 5px" }}>{article.title} | {article.category}</h4>
                                </div>
                            </Link>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default NewsArticalLists;


// server side call...
export const getServerSideProps: GetServerSideProps<{ articles: Array<newsModel> }> = async ({ }) => {
    const response = await fetch(`${jsonServerBaseUrl}/news`);
    const news: newsModel[] = await response.json();

    return {
        props: { articles: news }
    }
}