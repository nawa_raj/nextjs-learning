import React, { useEffect, useState } from 'react'
import { jsonServerBaseUrl } from 'services/config';

const IdCardIndexPage = () => {
    const data = { name: "nawaraj", aga: 25 };
    const [resData, setResData] = useState({} as any);


    useEffect(() => {
        const fetchData = async () => {
            const res = await fetch(`${jsonServerBaseUrl}/testid`);
            const resdata = await res.json();
            setResData(resdata);
        };

        fetchData();

    }, [])


    return (
        <div>{resData?.data}</div>
    )
}

export default IdCardIndexPage