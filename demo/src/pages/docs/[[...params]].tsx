import { useRouter } from 'next/router'
import React from 'react'


// [...params] "catch all routes" into a single routes except its index paege...
// [[...params]] "optional catch all" routes into a single routes...


// ----------------------------------------------------------------------------
const DocsIndexPage = () => {
    const router = useRouter();
    const { query: { params = [] }, pathname, asPath, route } = router;


    console.log("params: ", params)
    console.log("pathname: ", pathname);
    console.log("asPath: ", asPath);
    console.log("route: ", route);


    if (params.length === 2) {

        // this return like this: concept 1 => example 1
        return <h2>viewing docs for {params[0]} and its concept {params[1]}</h2>

    } else if (params.length === 1) {

        // this return like this: concept 1 
        return <h2>viewing docs for {params[0]}</h2>
    }

    return (
        <div>
            {/* this return all lists of the docs... */}
            <h1>This is a docs index page</h1>
        </div>
    )
}

export default DocsIndexPage;