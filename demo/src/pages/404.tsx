import React from 'react'



const PageNotFound = (error: any) => {

    // console.log("page not found params: ", error);

    return (
        <div
            style={{
                textAlign: "center",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                height: "100vh"
            }}
        >
            Opps!! page not found
        </div>
    )
}

export default PageNotFound