import React, { useState } from 'react'
import useSWR from 'swr'
import { jsonServerBaseUrl } from '../services/config'
import { dashboardModel } from './dashboard'



// -----------------------------------------------------------------------------------------
//  this example demonstrate client side data fetching with nextjs recommanded SWR package
// -----------------------------------------------------------------------------------------

const fetcher = (args: any) => fetch(args).then(res => res.json())

const DashboardSWR = () => {
    // const { data, error } = useSWR<dashboardModel>(`${jsonServerBaseUrl}/dashboard`, fetcher);
    const [data, setData] = useState<dashboardModel>();

    const loadData = async () => {
        const res = await fetch(`${jsonServerBaseUrl}/dashboard`);
        const data = await res.json() as dashboardModel;
        if (res.ok && data) setData(data);
    }


    return (
        <div>
            <div className='displayflex'>
                <h2>Dashboard</h2>
                <button onClick={loadData}>Load Data </button>
            </div>

            <hr />
            <div style={{ padding: "1rem" }}>
                <p>Posts: <strong>{data?.posts}</strong></p>
                <p>Likes: <strong>{data?.likes}</strong></p>
                <p>Followers: <strong>{data?.followers}</strong></p>
                <p>Following: <strong>{data?.following}</strong></p>
            </div>
        </div>
    )
}

export default DashboardSWR;