import React, { useEffect, useState } from 'react'
import { jsonServerBaseUrl } from '../services/config'



// -----------------------------------------------------------------------------
//  this example demonstrate client side data fetching with useEffect
// ------------------------------------------------------------------------------


// TYPE DECLERATION...
export interface dashboardModel {
    posts: number;
    likes: number;
    followers: number;
    following: number
}

const Dashboard = () => {
    const [dashboardData, setDashboardData] = useState<dashboardModel>();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        async function fetchDashboardData() {
            const response = await fetch(`${jsonServerBaseUrl}/dashboard`);
            const data = await response.json();
            if (response.ok && data) setDashboardData(data);
            setLoading(false);
        }

        fetchDashboardData()
    }, []);


    if (loading) return <div>loading...</div>

    return (
        <div>
            <h2>Dashboard</h2>
            <hr />
            <div style={{ padding: "1rem" }}>
                <p>Posts: <strong>{dashboardData?.posts}</strong></p>
                <p>Likes: <strong>{dashboardData?.likes}</strong></p>
                <p>Followers: <strong>{dashboardData?.followers}</strong></p>
                <p>Following: <strong>{dashboardData?.following}</strong></p>
            </div>
        </div>
    )
}

export default Dashboard