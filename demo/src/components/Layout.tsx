import Head from 'next/head'
import React, { FC } from 'react'
import DefaultHead from './DefaultHead'
import Footer from './Footer'
import FullHeader from './FullHeader'

const Layout: FC<{ children: React.ReactNode }> = ({ children }) => {
    return (
        <><DefaultHead />
            <FullHeader />
            <div style={{ minHeight: "calc(100vh - 105px)" }}>
                {children}
            </div>
            <Footer />
        </>
    )
}

export default Layout