import Head from 'next/head'
import React from 'react'

const DefaultHead = () => {
    return (
        <Head>
            <title>Demo Project</title>
            <link rel="icon" href="/favicon.ico" />
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <meta name="description" content="this is a demo project by nawaraj jaishi" />
        </Head>
    )
}

export default DefaultHead