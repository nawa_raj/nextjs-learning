import React, { FC } from 'react'
import DefaultHead from './DefaultHead'


// PROPS...
interface Props { children: React.ReactNode }


// -------------------------------------------
const GetLayout: FC<Props> = ({ children }) => {
    return (
        <>
            <DefaultHead />
            <div style={{ minHeight: "calc(100vh - 105px)" }}>{children}</div>
        </>
    )
}

export default GetLayout