import Link from 'next/link'
import React from 'react'
import style from "styles/Header.module.css"

const FullHeader = () => {
    return (
        <header className={style.header_container}>
            <h1 className={style.brand}>Demo Project</h1>
            <div className={`displayflex ${style.left_menu}`}>
                <Link href="/">Home</Link>
                <Link href="/dashboard">Dashbaord</Link>
                <p>Signin</p>
                <p>Signout</p>
            </div>
        </header>
    )
}

export default FullHeader