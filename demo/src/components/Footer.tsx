import React from 'react'

const Footer = () => {
    return (
        <footer
            className='displayflex'
            style={{
                padding: "1rem",
                justifyContent: "center",
                background: "#000000",
                color: "#f0f0f0"
            }}
        >
            this is a Footer
        </footer>
    )
}

export default Footer