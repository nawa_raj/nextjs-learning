"use client"        // this statement make this component as client component 
import React from 'react';


const BlogHomePage = () => {
    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        console.log("form event target: ", e.nativeEvent);
    }


    return (
        <div>
            this is a blog home page
            <form onSubmit={handleSubmit}>
                <div style={{ marginBottom: "0.5rem" }}>
                    <label htmlFor='name'>Name: </label>
                    <input type="text" name="name" />
                </div>

                <div style={{ marginBottom: "0.5rem" }}>
                    <label htmlFor='age'>Age: </label>
                    <input type="number" name="age" />
                </div>

                <button type='submit'> save </button>
            </form>
        </div>
    )
}

export default BlogHomePage