import './globals.css'

export default function RootLayout({ children, ...rest }: { children: React.ReactNode }) {

  return (
    <html lang="en">
      {/*
        <head /> will contain the components returned by the nearest parent
        head.tsx. Find out more at https://beta.nextjs.org/docs/api-reference/file-conventions/head
      */}
      <head />
      <body>
        <header> this is a header </header>

        <div style={{ padding: "1rem", margin: "1rem" }}>
          <h1 style={{ marginBottom: "1rem" }}>Following is the our body part </h1>

          {children}
        </div>

        <footer>
          this is a footer
        </footer>
      </body>
    </html>
  )
}
