import Link from 'next/link'
import React, { FC } from 'react'

const MarketingLayout: FC<{ children: React.ReactNode }> = ({ children, ...rest }) => {
    console.log("rest props: ", rest);


    return (
        <div>
            <header style={{
                background: "rgba(255, 255, 255, 0.5)",
                padding: "1rem",
                display: "flex",
                justifyContent: "space-between", alignItems: "center"
            }}>
                <Link href={"/"}>home</Link>
                <Link href={"/about"}>about</Link>
            </header>

            <div style={{ minHeight: "90vh" }}>
                {children}
            </div>


            <footer style={{ background: "rgba(0, 0, 0, 0.5)", padding: "1rem" }}>
                <p>logo section</p>
                <p>copuright section</p>
            </footer>
        </div>
    )
}

export default MarketingLayout